precmd () {print -Pn "\e]0;%~\a"} # update the window title to current dir
# Some useful options (man zhoptions)
setopt autocd nomatch extendedglob # menucomplete
setopt interactive_comments
stty stop undef
zle_highlight=('paste:none')
unsetopt BEEP

autoload -Uz compinit; compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Za-z}'
# zstyle ':completion::complete:lsof:*' menu yes select
zmodload zsh/complist
# compinit
_comp_options+=(globdots)       # Include hidden files

# [[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

source "$ZDOTDIR/zsh-functions"
zsh_add_file "zsh-exports"
zsh_add_file "zsh-vim-mode"
zsh_add_file "zsh-aliases"
zsh_add_file "zsh-prompt"
# zsh_add_file "colemakDH"

bindkey -s '^o' 'lfcd\n'
bindkey -s '^[o' 'lfcd\n'
bindkey -s '^[m' 'play-music\n'
bindkey -s '^[i' ' !! | bat\n'

zsh_add_plugin "zsh-users/zsh-syntax-highlighting"
zsh_add_plugin "zsh-users/zsh-autosuggestions"
# ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#f000ff"
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#6f828e"

eval "$(zoxide init --cmd cd zsh)"
eval `dircolors ~/.config/zsh/bliss.dircolors`

# bun completions
[ -s "/home/alegria/.bun/_bun" ] && source "/home/alegria/.bun/_bun"
