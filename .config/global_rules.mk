# $@: target file
# $<: source file

## For 64-bit, one would use:
## nasm -f elf64 main.asm
## ld -m elf_x86_64 main.o io.o -o main
%: %.asm
	nasm -f elf32 -i ~/.local/bin $< -o ${@}.o
	ld -m elf_i386 ~/.local/bin/io.o ${@}.o -o $@
	rm -f ${@}.o

