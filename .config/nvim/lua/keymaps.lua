local set = vim.keymap.set

vim.g.mapleader      = " "
vim.g.maplocalleader = "\\"

set( "", "<Space>", "<Nop>" )

set( "n", "<leader>c", ":nohl<CR>" )

--- Keep being in visual mode after pressing '<' or '>'
set( "v", "<", "<gv" )
set( "v", ">", ">gv" )

set("x", "<C-j>", ":move '>+1<CR>gv=gv")
set("x", "<C-k>", ":move '<-2<CR>gv=gv")

-- vim.keymap.set("n", "J", "mzJ`z") --- Keep cursor in fixed position when joining lines
set("n", "<C-f>", "<C-u>zz")
set("n", "<C-d>", "<C-d>zz")
--- <C-b> and <C-u> are available ( I dont use them often )

set("x", "<leader>y", [["+y]]) -- paste the previous thing yanked
set("x", "<leader>p", [["0p]]) -- paste the previous thing yanked

set("n", "n", "nzzzv")
set("n", "N", "Nzzzv")

set("n", "]q", "<cmd>cnext<CR>zz")
set("n", "[q", "<cmd>cprev<CR>zz")
set("n", "]l", "<cmd>lnext<CR>zz")
set("n", "[l", "<cmd>lprev<CR>zz")
set("n", "<C-j>", "<cmd>cnext<CR>zz")
set("n", "<C-k>", "<cmd>cprev<CR>zz")

-- vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- search and replace 'interactively'
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("x", "<leader>s", [[:s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
-- vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>//gI<Left><Left><Left>]])

