local o = vim.o

--- RANDOM
o.title         = true
o.termguicolors = true
o.syntax        = "off"
o.fileencoding  = "utf-8"
-- vim.opt.iskeyword:append("-") -- TODO: get ts-context-commentstring
o.history       = 50        -- Remember n items in the comandline history
o.pumheight     = 8         -- Popup menu height        -- Popup menu height
o.background    = "dark"
o.conceallevel  = 0   -- so that `` is visible in markdown files

-- vim.opt.fillchars.foldclose = "M"

-- o.clipboard     = "unnamedplus"
-- o.isfname = o.isfname .. "@-@" -- TODO
-- vim.opt.formatoptions:remove ({"c", "r", "o"}) -- TODO
-- o.shortmess = o.shortmess .. "c" -- Don't print ins-completion mesagges
-- o.jumpoptions    = "view" -- Preserve view while jumping TODO
-- o.hidden = true  -- Keep any editing buffer in the background TODO


--- GENERAL UI
o.showtabline   = 2
o.showmode       = false    -- Dont show the current mode
o.cursorline     = true     -- Highlight current line
o.number         = true     -- Show current line number
o.numberwidth    = 4
o.signcolumn     = "yes"    -- Add a column on the left for linting and errors (number, yes:2)
o.relativenumber = true     -- Show line numbers
o.wrap           = true     -- Wrap text that extends beyond the screen
o.scrolloff      = 8        -- Start scrolling from 'n' lines away
o.colorcolumn    = "81"     -- Put a column at column #80
o.cmdheight     = 1
-- vim.opt.fillchars = { eob = " " } -- Disable `~` on nonexistent lines

--- SEARCH
o.incsearch  = true  -- Highlight words as searching
o.hlsearch   = true  -- Highlight all found words
o.ignorecase = true  -- Case-insensitive searching
o.smartcase  = true  -- Case-sensitive searching when using capital letters

--- TABS
o.expandtab   = true -- Converts tabs to spaces
o.autoindent  = true
o.smartindent = true
o.tabstop     = 4
o.softtabstop = 4
o.shiftwidth  = 4
o.smarttab    = true

--- FOLDS  TODO:
-- o.foldmethod = 'indent'
-- o.foldlevelstart = 99
-- o.foldnestmax = 3
-- o.foldminlines = 1

--- Undo and backup options
o.backup      = false
o.swapfile    = false
o.writebackup = false
o.undofile    = true
o.undodir     = os.getenv("HOME") .. "/.local/state/nvim/undodir"

-- Better buffer splitting
o.splitright = true
o.splitbelow = true
-- o.splitkeep = 'screen' --TODO
-- o.diffopt:append('linematch:60') --TODO

--- Decrease update time
o.timeoutlen = 1500;
o.updatetime = 300;

--- Extra characters to show in the file
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ "

o.linebreak = true
o.breakindent = true -- Wrap indent to match  line start
o.laststatus = 3

