
-- Highlight on yank
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = vim.api.nvim_create_augroup('YankHighlight', { clear = true }),
  pattern = '*',
})

--- Remove trailing white space on save
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  pattern = {"*"},
  callback = function()
    local sv = vim.fn.winsaveview()
    pcall(function() vim.cmd [[%s/\s\+$//e]] end)
    vim.fn.winrestview(sv)
  end,
})

--- fix nvim's behaviour of identifying *.h files as *.cpp files
vim.cmd[[autocmd BufNewFile,BufRead *.h setlocal filetype=c]]
vim.cmd[[autocmd FileType html,css,vue,js,ts setlocal tabstop=2 softtabstop=2 shiftwidth=2]]

