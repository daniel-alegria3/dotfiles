return {
  "nvim-telescope/telescope.nvim",

  dependencies = {
    {"nvim-telescope/telescope-fzf-native.nvim", build = "make"},
  },

  config = function()
    local actions = require("telescope.actions")

    require("telescope").setup({
      defaults = {
        sorting_strategy = "ascending",
        scroll_strategy = "limit",
        layout_config = {
          prompt_position = "top",
        },

        -- Interface keybinds
        mappings = {
          i = {
            ["<C-j>"] = actions.cycle_history_next,
            ["<C-k>"] = actions.cycle_history_prev,

            ["<C-n>"] = actions.move_selection_next,
            ["<C-p>"] = actions.move_selection_previous,

            ["<C-d>"] = actions.preview_scrolling_down,
            ["<C-f>"] = actions.preview_scrolling_up,

            -- ["<PageUp>"] = actions.results_scrolling_up,
            -- ["<PageDown>"] = actions.results_scrolling_down,

            -- ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
            -- ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
            -- ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
            -- ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
            -- ["<C-l>"] = actions.complete_tag,
          },

          n = {

            ["<C-d>"] = actions.preview_scrolling_down,
            ["<C-f>"] = actions.preview_scrolling_up,

            -- ["<PageDown>"] = actions.results_scrolling_down,
            -- ["<PageUp>"] = actions.results_scrolling_up,

            -- ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
            -- ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
            -- ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
            -- ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
          },
        },
      }
    })

    require("telescope").load_extension("fzf")

    --- keymaps
    local builtin = require("telescope.builtin")
    local opts = {}
    vim.keymap.set('n', '<leader>ff', builtin.find_files, opts)
    vim.keymap.set('n', '<leader>fg', builtin.live_grep, opts)
    vim.keymap.set('n', '<leader>fb', builtin.buffers, opts)
    vim.keymap.set('n', '<leader>fh', builtin.help_tags, opts)
    vim.keymap.set('n', '<leader>f:', builtin.command_history, opts)
    vim.keymap.set('n', '<leader>f/', builtin.search_history, opts)
    vim.keymap.set('n', '<leader>f?', builtin.oldfiles, opts)
    vim.keymap.set('n', '<leader>fm', builtin.man_pages, opts)
    vim.keymap.set('n', '<leader>fq', builtin.quickfix, opts)
    vim.keymap.set('n', '<leader>fd', builtin.diagnostics, opts)
    -- vim.keymap.set('n', '<leader>xx', builtin.highlights, opts)
    -- vim.keymap.set('n', '<leader>xx', builtin.git_files, {})
    vim.keymap.set('n', '<leader>fs', function()
      builtin.grep_string({ search = vim.fn.input("Grep > ") })
    end)
  end,
}

