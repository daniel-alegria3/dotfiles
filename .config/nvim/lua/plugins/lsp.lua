local servers = {
  "clangd",
  "pyright",
  "lua_ls",
  "rust_analyzer",
  "tsserver",
  "gopls",
  "cssls",
  "html",
  "jsonls",
  "volar",
  "emmet_language_server",
  "asm_lsp",
  ---
  -- "ltex",
  "texlab",
  --- Temp
  "csharp_ls",
  "matlab_ls",
}

--- lsp, linting and formating
return {
  { "neovim/nvim-lspconfig",
    dependencies = {
      "williamboman/mason-lspconfig.nvim",
      "hrsh7th/cmp-nvim-lsp",
    },
    config = function()
      local on_attach = function(client, bufnr)
        if client.name == "tsserver" then
          client.server_capabilities.documentFormattingProvider = false
        end

        if client.name == "lua_ls" then
          client.server_capabilities.documentFormattingProvider = false
        end

        -- keybinds
        local opts = { noremap = true, silent = true, buffer = bufnr }
        local keymap = vim.keymap.set
        keymap("n", "gD", vim.lsp.buf.declaration, opts)
        keymap("n", "gd", vim.lsp.buf.definition, opts)
        keymap('n', "<leader>gd", vim.lsp.buf.type_definition, opts) -- "<leader>D"
        keymap("n", "K", vim.lsp.buf.hover, opts)
        keymap("n", "gI", vim.lsp.buf.implementation, opts) -- "gi"
        keymap("n", "gr", vim.lsp.buf.references, opts)
        keymap("n", "gl", vim.diagnostic.open_float, opts) -- '<space>e'
        keymap("n", "[d", vim.diagnostic.goto_prev, opts) -- '<leader>lp'
        keymap("n", "]d", vim.diagnostic.goto_next, opts) -- '<leader>ln'
        keymap("n", "<leader>la", vim.lsp.buf.code_action, opts) -- '<space>ca'
        keymap("n", "<leader>lr", vim.lsp.buf.rename, opts) -- '<space>rn'
        keymap("i", "<C-k>", vim.lsp.buf.signature_help, opts) -- '<C-k>'
        keymap("n", "<leader>gl", vim.diagnostic.setqflist, opts)  -- '<space>q'
        keymap('n', '<leader>lf', function() vim.lsp.buf.format { async = true } end, opts)

        -- WORSPACE STUFF
        --- '<leader>w' is taken by harpoon, 'v' is the closest thing, I guess
        -- keymap('n', '<space>va', vim.lsp.buf.add_workspace_folder, opts)
        -- keymap('n', '<space>vr', vim.lsp.buf.remove_workspace_folder, opts)
        -- keymap('n', '<space>vl', function()
        --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        -- end, opts)
        -- keymap("n", "<leader>vs", vim.lsp.buf.workspace_symbol, opts)
        -- vim.lsp.buf.format
      end

      local capabilities
      capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities.textDocument.completion.completionItem.snippetSupport = true
      capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities) --- CMP_NVIM_LSP

      local opts = {
        on_attach = on_attach,
        capabilites = capabilities,
      }

      local lspconfig = require("lspconfig")
      local status_ok, conf_opts
      for _, server in pairs(servers) do
        status_ok, conf_opts = pcall( require, "lsp_settings." .. server )
        if status_ok then
          conf_opts = vim.tbl_deep_extend("force", conf_opts, opts)
        else
          conf_opts = opts
        end
        -- server = vim.split(server, "@")[1]
        lspconfig[server].setup(conf_opts)
      end

      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
        border = "rounded",
      })

      vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
        border = "rounded",
      })

      -- Setup how lsp looks
      --[[
      MOVED THIS SECtION TO init.lua
      --]]--
    end,
  },

  { "williamboman/mason-lspconfig.nvim",
    dependencies = "williamboman/mason.nvim",
    config = function()
      require("mason").setup({
        ui = {
          border = "rounded"
        },
        max_concurrent_installers = 4,
      })
      require("mason-lspconfig").setup({
        ensure_installed = servers,
        -- automatic_installation = true,
      })
    end,
  },

  { "jay-babu/mason-null-ls.nvim",
    dependencies = {
      "williamboman/mason.nvim",
      "jose-elias-alvarez/null-ls.nvim",
      -- "nvimtools/none-ls.nvim",
    },

    event = { "BufReadPre", "BufNewFile" },

    config = function()
      ---- NULL_LS (see the null-ls documentation to see hot to autoformat on save of a buffer)
      require("mason-null-ls").setup({
        ensure_installed = {
          -- "clang-format",
          "prettier",
          "stylua",
          "eslint_d",
          "black",
          "flake8",
        }
      })

      local null_ls = require("null-ls")

      -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
      local formatting = null_ls.builtins.formatting
      -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
      local diagnostics = null_ls.builtins.diagnostics

      -- https://github.com/prettier-solidity/prettier-plugin-solidity
      null_ls.setup {
        debug = false,
        sources = {
          formatting.prettier.with {
            extra_filetypes = { "toml" },
            extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" },
          },
          formatting.black.with { extra_args = { "--fast" } },
          formatting.stylua,
          formatting.google_java_format,
          diagnostics.flake8,
        },
      }
    end,
  },

  { "j-hui/fidget.nvim",
    opts = {};
  },
  { "luckasRanarison/clear-action.nvim",
    opts = {},
  },

}
