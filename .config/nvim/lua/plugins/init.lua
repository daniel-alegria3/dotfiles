return {
  "nvim-lua/plenary.nvim",

  --- Better workflow
  "ThePrimeagen/vim-be-good",
  "mbbill/undotree",
  -- "folke/trouble.nvim",

  --- Miscellaneous standalones
  "tpope/vim-unimpaired",
  "nvim-tree/nvim-web-devicons",

  -- "LucasTavaresA/simpleIndentGuides.nvim",
  -- "lukas-reineke/indent-blankline.nvim",
  -- "jiangmiao/auto-pairs",
  -- "windwp/auto-pairs",
}

