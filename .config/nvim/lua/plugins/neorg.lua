--- Note taking
return {
  "nvim-neorg/neorg",

  dependencies = {
    "nvim-neorg/lua-utils.nvim",
    "nvim-neotest/nvim-nio",
    "MunifTanjim/nui.nvim",
    "nvim-lua/plenary.nvim",
    "pysan3/pathlib.nvim",
  },

  lazy = true,
  ft = "norg",
  build = ":Neorg sync-parsers",

  opts = {
    load = {
      -- Loads default behaviour
      ["core.defaults"] = {},
      -- Adds pretty icons to your documents
      ["core.concealer"] = {
        config = {
          icon_preset = "diamond"
        }
      },
      ["core.export"] = {},
    }
  },
}

