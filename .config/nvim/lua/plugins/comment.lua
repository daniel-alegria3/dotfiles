return {
  { "terrortylor/nvim-comment",
    config = function()
      require("nvim_comment").setup({})
      --- General
      vim.cmd("autocmd FileType c,cpp,cs,cu,java setlocal commentstring=//%s")
      vim.cmd("autocmd FileType sql setlocal commentstring=--%s")
      -- vim.cmd("autocmd FileType gd,gdscript setlocal commentstring=# %s")
    end
  },

  { "JoosepAlviste/nvim-ts-context-commentstring",
    config = function()
      require("ts_context_commentstring").setup({})
    end,
  },

}

