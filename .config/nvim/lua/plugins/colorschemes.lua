--- Colorschemes
return {
  {
    "ellisonleao/gruvbox.nvim",
    "neanias/everforest-nvim",
    "sainnhe/gruvbox-material",
    "sainnhe/edge",
    lazy = true,
  },

  { "catppuccin/nvim",
    lazy = false,
    config = function()
      require("catppuccin").setup({
        dim_inactive = {
          enabled = true,
          shade = "dark",
          percentage = 0.15,
        },
      })

      -- fav default colors: habamax, peachpuff, slate, desert, evening, lunaperche,
      vim.cmd("colorscheme catppuccin-mocha")
      -- vim.cmd("colorscheme gruvbox")
    end,
  },

}

