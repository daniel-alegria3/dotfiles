--- Treesitter
return {
  { "nvim-treesitter/nvim-treesitter-context",
    dependencies = {"nvim-treesitter/nvim-treesitter"}
  },

  { "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      require("nvim-treesitter.configs").setup({
        ensure_installed = {
          "c",
          "cpp",
          "lua",
          "python",
          "typescript",
          "javascript",
          "html",
          "css",
          "vue",
          "markdown",
          "bash",
          "rust",
          "go",
        },
        -- ignore_install = { "" }
        -- autoinstall = true,
        highlight = {
          enable = true,
          disable = { "" },
          additional_vim_regex_highlighting = false,
        },
        indent = {
          enabled = true,
          disable = { "" }, -- i saw two peaple with python disabled
        },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = '<c-space>',
            node_incremental = '<c-space>',
            scope_incremental = '<c-s>',
            node_decremental = '<M-space>',
          },
        },

        -- autotag = { enabled = true } -- requires the autotag plugin
        -- ts_context_commentstring = { enable = true, enable_autocmd = false, },
        ts_context_commentstring = { enable = true },
      })

      -- [[ Configure Treesitter ]]
      -- See `:help nvim-treesitter`
      --
      -- require('nvim-treesitter.configs').setup {
      --   -- Add languages to be installed here that you want installed for treesitter
      --   ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'tsx', 'typescript', 'vimdoc', 'vim' },
      --
      --   -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
      --   auto_install = false,
      --
      --   highlight = { enable = true },
      --   indent = { enable = true, disable = { 'python' } },
      --   incremental_selection = {
      --   enable = true,
      --   keymaps = {
      --     init_selection = '<c-space>',
      --     node_incremental = '<c-space>',
      --     scope_incremental = '<c-s>',
      --     node_decremental = '<M-space>',
      --   },
      --   },
      --   textobjects = {
      --   select = {
      --     enable = true,
      --     lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      --     keymaps = {
      --     -- You can use the capture groups defined in textobjects.scm
      --     ['aa'] = '@parameter.outer',
      --     ['ia'] = '@parameter.inner',
      --     ['af'] = '@function.outer',
      --     ['if'] = '@function.inner',
      --     ['ac'] = '@class.outer',
      --     ['ic'] = '@class.inner',
      --     },
      --   },
      --   move = {
      --     enable = true,
      --     set_jumps = true, -- whether to set jumps in the jumplist
      --     goto_next_start = {
      --     [']m'] = '@function.outer',
      --     [']]'] = '@class.outer',
      --     },
      --     goto_next_end = {
      --     [']M'] = '@function.outer',
      --     [']['] = '@class.outer',
      --     },
      --     goto_previous_start = {
      --     ['[m'] = '@function.outer',
      --     ['[['] = '@class.outer',
      --     },
      --     goto_previous_end = {
      --     ['[M'] = '@function.outer',
      --     ['[]'] = '@class.outer',
      --     },
      --   },
      --   swap = {
      --     enable = true,
      --     swap_next = {
      --     ['<leader>a'] = '@parameter.inner',
      --     },
      --     swap_previous = {
      --     ['<leader>A'] = '@parameter.inner',
      --     },
      --   },
      --   },
      -- }
    end,
  },
}
