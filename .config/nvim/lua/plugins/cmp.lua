--- Completion engine
return {
  "hrsh7th/nvim-cmp",

  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    { "dcampos/cmp-snippy", dependencies = "dcampos/nvim-snippy" },
  },

  config = function()
    local cmp = require("cmp")

    --   פּ ﯟ   some other good icons
    local kind_icons = {
      Text = "",
      Method = "m",
      Function = "",
      Constructor = "",
      Field = "",
      Variable = "",
      Class = "",
      Interface = "",
      Module = "",
      Property = "",
      Unit = "",
      Value = "",
      Enum = "",
      Keyword = "",
      Snippet = "",
      Color = "",
      File = "",
      Reference = "",
      Folder = "",
      EnumMember = "",
      Constant = "",
      Struct = "",
      Event = "",
      Operator = "",
      TypeParameter = "",
    }
    -- find more here: https://www.nerdfonts.com/cheat-sheet


    cmp.setup({
        completion = { completeopt = 'menu,menuone,noinsert' },
        snippet = {
          expand = function(args)
            require('snippy').expand_snippet(args.body)
          end,
        },
        -- keybinds
        mapping = cmp.mapping.preset.insert({
          ['<C-n>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
          ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
          ['<C-d>'] = cmp.mapping.scroll_docs(4),
          ['<C-f>'] = cmp.mapping.scroll_docs(-4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<C-e>'] = cmp.mapping.abort(),
          -- Accept currently selected item.
          -- Set `select` to `false` to only confirm explicitly selected items.
          ['<Tab>'] = cmp.mapping.confirm({ select = true, behavior=cmp.ConfirmBehavior.Insert }),
        }),
        sources = {
          { name = 'nvim_lsp', max_item_count = 10 },
          { name = 'snippy'  , max_item_count = 10 },
          { name = 'buffer'  , max_item_count = 10 },
          { name = 'path'    , max_item_count = 10 },
        },
        formatting = {
          -- fields = { "kind", "abbr", "menu" },
          format = function(entry, vim_item)
            -- Kind icons
            -- vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
            vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
            vim_item.menu = ({
              nvim_lsp = "[lsp]",
              snippy   = "[snippy]",
              buffer   = "[buffer]",
              path     = "[path]",
              cmdline  = "[cmdline]",
            })[entry.source.name]
            return vim_item
          end,
        },
        window = {
            -- completion = cmp.config.window.bordered(),
            documentation = cmp.config.window.bordered(),
        },
        view = {
            entries = { name = "custom", selection_order = "near_cursor" }
        },
        cmp.native_menu
    })

    -- -- Set configuration for specific filetype.
    -- cmp.setup.filetype('gitcommit', {
    --     sources = cmp.config.sources({
    --         { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    --     }, {
    --         { name = 'buffer' },
    --     })
    -- })
    --
    -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
    cmp.setup.cmdline({ '/', '?' }, {
      completion = { completeopt = 'menu,menuone,noselect' },
      mapping = cmp.mapping.preset.cmdline(),
      -- view = { entries = { name = "wildmenu", separator = " | " } },
      sources = {
          { name = "buffer" }
      }
    })

    -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
    cmp.setup.cmdline(':', {
      completion = { completeopt = 'menu,menuone,noselect' },
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = 'path' }
      }, {
        { name = 'cmdline' }
      }),
      matching = { disallow_symbol_nonprefix_matching = false }
    })

    --- NOTE: The following is called in lsp.lua
    -- Set up lspconfig.
    -- local capabilities = require('cmp_nvim_lsp').default_capabilities()
    -- -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
    -- require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
    --   capabilities = capabilities
    -- }

    ---- Fix weird bug I encountered
    ---- https://www.reddit.com/r/neovim/comments/ucze6k/q_any_idea_why_i_cannot_do_tab_completion_on/
    vim.keymap.set('c', '<tab>', '<C-z>', { silent = false })
  end,
}

