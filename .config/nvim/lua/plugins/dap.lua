--- debugging
return {
  "rcarriga/nvim-dap-ui",

  dependencies = {
    "mfussenegger/nvim-dap",
    "nvim-neotest/nvim-nio",
  },

  config = function()
    local dap = require("dap")
    local dap_widgets = require("dap.ui.widgets")

    local set = vim.keymap.set
    --- Installed debuggers (via Mason)
    -- debugpy
    -- cpptools (C/C++ IntelliSense, debugging, and code browsing)
    -- codelldb (A native debugger powered by LLDB.  Debug C++, Rust and other compiled languages)
    -- netcoredbg ( C# debugger )
    -- js-debug-adapter
    -- go-debug-adapter

    -- dap specific language configurations
    dap.adapters.cppdbg = {
      id = 'cppdbg',
      type = 'executable',
      command = vim.fn.stdpath("data") .. "/mason/packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7",
    }
    dap.configurations.cpp = {
      {
        name = "Launch file",
        type = "cppdbg",
        request = "launch",
        program = function()
          return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopAtEntry = true,
      },
      -- {
      --   name = 'Attach to gdbserver :1234',
      --   type = 'cppdbg',
      --   request = 'launch',
      --   MIMode = 'gdb',
      --   miDebuggerServerAddress = 'localhost:1234',
      --   miDebuggerPath = '/usr/bin/gdb',
      --   cwd = '${workspaceFolder}',
      --   program = function()
      --     return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
      --   end,
      -- },
    }
    dap.configurations.c = dap.configurations.cpp
    dap.configurations.rust = dap.configurations.cpp

    dap.adapters.coreclr = {
      type = 'executable',
      command = '/path/to/dotnet/netcoredbg/netcoredbg',
      args = {'--interpreter=vscode'}
    }
    dap.configurations.cs = {
      {
        type = "coreclr",
        name = "launch - netcoredbg",
        request = "launch",
        program = function()
          return vim.fn.input('Path to dll', vim.fn.getcwd() .. '/bin/Debug/', 'file')
        end,
      },
    }

    dap.adapters.python = {
      type = "executable",
      -- command = "/usr/bin/python",
      command = vim.fn.stdpath("data") .. "/mason/packages/debugpy/venv/bin/python",
        args = { "-m", "debugpy.adapter" },
      }
    dap.configurations.python = {
      {
        type = "python",
        name = "launch file",
        request = "launch",
        program = "${file}",
        pythonpath = function()
          local cwd = vim.fn.getcwd()
          if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
            return cwd .. "/venv/bin/python"
          elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
            return cwd .. "/.venv/bin/python"
          else
            return "/usr/bin/python"
          end
        end,
      },
    }

    --- dap icons
    local signs = {
      { name = "DapStopped",             text = "",  texthl = "DiagnosticInfo" },
      { name = "DapBreakpoint",          text = "",  texthl = "DiagnosticWarn" },
      { name = "DapBreakpointCondition", text = "",  texthl = "DiagnosticWarn" },
      { name = "DapBreakpointRejected",  text = "",  texthl = "DiagnosticError" },
      { name = "DapLogPoint",            text = ".>", texthl = "DiagnosticWarn" },
    }
    for _, sign in ipairs(signs) do
      vim.fn.sign_define(sign.name, sign)
    end

    --- dap-ui
    local dapui = require("dapui")

    dap.listeners.after.event_initialized["dapui_config"] = function()
      dapui.open()
    end
    dap.listeners.before.event_terminated["dapui_config"] = function()
      dapui.close()
    end
    dap.listeners.before.event_exited["dapui_config"] = function()
      dapui.close()
    end

    dapui.setup({
      expand_lines = true,
      icons = { expanded = "", collapsed = "", circular = "" },
      mappings = {
        -- Use a table to apply multiple mappings
        expand = { "<CR>", "<2-LeftMouse>" },
        open   = "o",
        remove = "d",
        edit   = "e",
        repl   = "r",
        toggle = "t",
      },
      -- Use this to override mappings for specific elements
      element_mappings = {},
      layouts = {
        {
          elements = {
            { id = "scopes", size = 0.33 },
            { id = "breakpoints", size = 0.17 },
            { id = "stacks", size = 0.25 },
            { id = "watches", size = 0.25 },
          },
          size = 0.33,
          position = "right",
        },
        {
          elements = {
            { id = "repl", size = 0.45 },
            { id = "console", size = 0.55 },
          },
          size = 0.27,
          position = "bottom",
        },
      },
      floating = {
        max_height = 0.9,
        max_width = 0.5, -- Floats will be treated as percentage of your screen.
        border = "rounded",
        mappings = {
          close = { "q", "<Esc>" },
        },
      },
      windows = { indent = 1 },
      render = {
        max_type_length = nil, -- Can be integer or nil.
        max_value_lines = 100, -- Can be integer or nil.
      },
    })

    --- dap/dapui keybindings
    set("n", "<F5>",      dap.continue )
    set("n", "<F4>",      dap.terminate )
    set("n", "<F8>",      dap.step_over )
    set("n", "<F9>" ,     dap.step_into )
    set("n", "<F10>",     dap.step_out )
    set("n", "<leader>b", dap.toggle_breakpoint )
    set("n", "<leader>B",  function()
      dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
    end)
    set("n", "<leader>dp", function() -- "<leader>lp"
      dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
    end)
    set("n",        "<leader>dr", dap.repl.toggle )
    set("n",        "<leader>dl", dap.run_last )
    set("n",        "<leader>de", dapui.eval )
    set("n",        "<leader>dw", dapui.elements.watches.add )
    set("n",        "<leader>df", dapui.float_element )
    set("n",        "<leader>dt", function ()
      dapui.toggle( {reset = true} )
    end )
    set("n",        "<leader>ds", function()
      dapui.float_element("scopes")
    end)
    -- set("n",        "<leader>xx", dapui.float_element)

    ---- dap_widgets are kinda bad ngl
    -- set({"n", "v"}, "<Leader>dh", dap_widgets.hover )
    -- set({"n", "v"}, "<Leader>dp", dap_widgets.preview )
    -- set("n",        "<Leader>df", function()
    --   dap_widgets.centered_float(dap_widgets.frames)
    -- end)
    -- set("n",        "<Leader>ds", function()
    --   dap_widgets.centered_float(dap_widgets.scopes)
    -- end)
  end,
}

