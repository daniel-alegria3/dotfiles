return {
  "Darazaki/indent-o-matic",

  opts = {
    -- The values indicated here are the defaults

    -- Number of lines without indentation before giving up (use -1 for infinite)
    max_lines = 2048,

    -- Space indentations that should be detected
    standard_widths = { 2, 4, 8 },

    -- Skip multi-line comments and strings (more accurate detection but less performant)
    skip_multiline = true,

    -- Disable indent-o-matic for vue files
    filetype_vue = {
      max_lines = 0,
    },

    -- Don't detect 8 spaces indentations inside files without a filetype
    filetype_ = {
      standard_widths = { 2, 4 },
    },

    -- Only detect 4 spaces and tabs for Rust files
    filetype_c = {
      standard_widths = { 4 },
    },
  },
}

