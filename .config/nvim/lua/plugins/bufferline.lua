--- Buffer line
return {
  "crispgm/nvim-tabline",
  --- Other options:
  -- "rafcamlet/tabline-framework.nvim",

  dependencies = { "nvim-tree/nvim-web-devicons" },
  event = "VimEnter",

  opts = {
    show_index  = false,       -- show tab index
    show_modify = true,        -- show buffer modification indicator
    show_icon   = true,        -- show file extension icon
    modify_indicator = '+',   -- modify indicator
    no_name     = 'No name',   -- no name buffer name
    brackets    = { '', '' }, -- file name brackets surrounding
  },
}

