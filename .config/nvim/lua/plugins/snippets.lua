-- snippets
return {
 "dcampos/nvim-snippy",

  dependencies = {
    "rafamadriz/friendly-snippets",
    "smjonas/snippet-converter.nvim"
  },

  ft = 'snippets',
  cmd = { 'SnippyEdit', 'SnippyReload' },

  config = function()
    vim.g.tex_flavor = "latex"
    require("snippy").setup({
      mappings = {
        is = {
          ['<C-j>'] = 'expand_or_advance',
          ['<C-k>'] = 'previous',
        },
        nx = {
          ['<leader>x'] = 'cut_text',
        },
      },
    })
  end,

  build = function ()
    local data_path = vim.fn.stdpath('data')
    local pets = data_path .. '/lazy/friendly-snippets/snippets'

    local snippets_chosen = {
      ["c"] = { pets .. '/c', },
      ["cpp"] = { pets .. '/cpp', },
      ["lua"] = { pets .. '/lua', },
      ["rust"] = { pets .. '/rust', },
      ["python"] = { pets .. '/python', },
      -- TODO: separate javascript, react, typescript snippets into their own
      ["javascript"] = { pets .. '/javascript', },
      ["tex"] = {
        pets .. '/latex/latex-snippets.json',
        pets .. '/latex/vscode-latex-snippets.json',
        pets .. '/latex.json',
      },
      ["bib"] = {
        pets .. '/latex/bibtex.json',
      },
      [""] = { -- means just single files
        pets .. '/julia.json',
        pets .. '/go.json',
        pets .. '/objc.json',
        pets .. '/r.json',
        pets .. '/scala.json',
        pets .. '/svelte.json',
        pets .. '/swift.json',
        pets .. '/tcl.json',

        pets .. '/solidity.json',
        pets .. '/purescript.json',
        pets .. '/reason.json',
        pets .. '/glsl.json',
        pets .. '/kubernetes.json',
        pets .. '/mint.json',
        pets .. '/licence.json',
        pets .. '/terraform.json',
        pets .. '/systemverilog.json',
        pets .. '/verilog.json',
        pets .. '/vhdl.json',

        pets .. '/html.json',
        pets .. '/css.json',
        pets .. '/markdown.json',
        pets .. '/rmarkdown.json',

        pets .. '/gdscript.json',
        pets .. '/plantuml.json',

        pets .. '/make.json',
        pets .. '/gitcommit.json',
        pets .. '/sql.json',
        pets .. '/norg.json',
      },
    }


    local top = 1
    local templates = {}
    for key, value in pairs(snippets_chosen) do
      templates[top] = {
        sources = { vscode = value },
        output = { snipmate = { data_path..'/site/snippets/'..key } },
                             -- vim.fn.stdpath('config')..'/snippets',
      }
      top = top + 1
    end

    require('snippet_converter').setup({
      templates = templates
    })
    vim.cmd(":ConvertSnippets")
  end,
}
