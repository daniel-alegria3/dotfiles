return {
  "ThePrimeagen/harpoon",

  dependencies = { "nvim-lua/plenary.nvim" },
  branch = "harpoon2",

  config = function()
    local harpoon = require("harpoon")
    local set = vim.keymap.set

    harpoon:setup()

    set("n", "<leader>a", function()
      harpoon:list():add()
    end)
    set("n", "<leader><leader>", function()
      harpoon.ui:toggle_quick_menu(harpoon:list())
    end)

    set("n", "<leader>q", function() harpoon:list():select(1) end )
    set("n", "<leader>w", function() harpoon:list():select(2) end )
    set("n", "<leader>e", function() harpoon:list():select(3) end )
    set("n", "<leader>r", function() harpoon:list():select(4) end )
    set("n", "<leader>t", function() harpoon:list():select(5) end )
  end,
}

