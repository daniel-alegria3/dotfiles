return {
  "preservim/vimux",

  config = function()
    local set = vim.keymap.set

    -- Prompt for a command to run
    set("n", "<leader>vp", "<cmd>VimuxPromptCommand<cr>")
    -- Run last command executed by VimuxRunCommand
    set("n", "<leader>vl", "<cmd>VimuxRunLastCommand<cr>")
    -- Inspect runner pane
    set("n", "<leader>vi", "<cmd>VimuxInspectRunner<cr>")
    -- Close vim tmux runner opened by VimuxRunCommand
    set("n", "<leader>vq", "<cmd>VimuxCloseRunner<cr>")
    -- Interrupt any command running in the runner pane
    set("n", "<leader>vc", "<cmd>VimuxInterruptRunner<cr>")
    -- Zoom the runner pane (use <bind-key> z to restore runner pane)
    set("n", "<leader>vz", "<cmd>VimuxZoomRunner<cr>")
    -- Clear the terminal screen of the runner pane.
    set("n", "<leader>v<C-l>", "<cmd>VimuxClearTerminalScreen<cr>")

    -- autorun last command on save
    vim.api.nvim_create_autocmd({ "BufWritePre" }, {
      pattern = {"*"},
      callback = function()
        pcall(function()
          vim.cmd("silent! VimuxInterruptRunner")
          vim.cmd("silent! VimuxRunLastCommand")
        end)
      end,
    })
  end,
}

