return {
  settings = {
    ltex = {
      checkFrequency = 'save',
      -- language = { "en-US", 'es' },
      language = 'es',
      additionalRules = {
        enablePickyRules = true,
        motherTongue = 'es',
      },

      -- setenceCacheSize = 2000,
      -- disabledRules = {},
      -- hiddenFalsePositives = {},
    },
  },
}
