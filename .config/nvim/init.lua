require("options")
require("keymaps")
require("plugins")
require("autocmds")
-- require("convert_friendly_snippets")

--/ Netrw
-- set( "n", "<leader>e", ":Lex 30<CR>" )
vim.cmd("let g:netrw_banner       = 0 ")
vim.cmd("let g:netrw_liststyle    = 3 ")
vim.cmd("let g:netrw_preview      = 1 ")
vim.cmd("let g:netrw_browse_split = 0 ")
-- vim.cmd("let g:netrw_winsize      = 20 ")

-- THIS WAS IN 'lua/plugins/lsp.lua' BUT IT WAS FAILING
local signs = {
  { name = "DiagnosticSignError", text = "", texthl = "DiagnosticSignError" },
  { name = "DiagnosticSignWarn",  text = "", texthl = "DiagnosticSignWarn" },
  { name = "DiagnosticSignHint",  text = "", texthl = "DiagnosticSignHint" },
  { name = "DiagnosticSignInfo",  text = "", texthl = "DiagnosticSignInfo" },
}

for _, sign in ipairs(signs) do
  vim.fn.sign_define( sign.name, sign )
end

vim.diagnostic.config({
  virtual_text = false, -- disable virtual text
  signs = {
    active = signs, -- show signs
  },
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = true,
    style =  "minimal",
    border = "single",
    source = "always",
    header = "",
    prefix = "",
  },
})

