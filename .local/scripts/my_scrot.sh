#! /usr/bin/env sh
# Stupid wrapper script to add some functionality to 'scrot'
function beep {
    ( speaker-test -t sine -f 512 )& pid=$! ; sleep 0.1s ; kill -9 $pid
}
 
savefile_opt="$HOME/Pictures/%Y-%m-%d_%H:%M:%S.png"
clipboard_opt=""

window_mode="--focused"

### if script has no arguments, default saves screenshot of current window 
for arg in "$@"; do
    case "$arg" in
        --fullscreen) window_mode="" ;;
        --selection)  window_mode="--select" ;;
        --clipboard)
            clipboard_opt="'/tmp/%Y-%m-%d_%H:%M:%S.png' -e "
            clipboard_opt+="'xclip -selection clipboard -target image/png -i \$f'"
            savefile_opt=""
            ;;
    esac
done

sh -c "scrot $window_mode $savefile_opt $clipboard_opt"

