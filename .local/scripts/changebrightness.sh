#!/usr/bin/env bash
# https://github.com/ericmurphyxyz/dotfiles/blob/master/.local/bin/changebrightness

function send_notification() {
    brightness=$(printf "%.0f\n" $(brillo -G))
    dunstify -a "changebrightness" -u low -r 9991 -h int:value:"$brightness" "Brightness: $brightness%" -t 1000
}

case $1 in
up)
    brillo -A 5 -q -e
    send_notification $1
    ;;
down)
    brillo -U 5 -q -e
    send_notification $1
    ;;
esac

