#!/usr/bin/env sh
# source: https://www.atlassian.com/git/tutorials/dotfiles
git clone --bare https://gitlab.com/daniel-alegria3/dotfiles.git $HOME/.dotfiles
function config {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
else
  echo "_____ Backing up pre-existing dot files ______";
  git clone https://gitlab.com/daniel-alegria3/dotfiles.git $HOME/config_backup
  \rm -rf $HOME/config_backup/.git
  # config checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} trash-put {}
  config checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} mv {} config_backup/{}
fi;
config checkout
config config status.showUntrackedFiles no
