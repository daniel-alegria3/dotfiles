#!/usr/bin/env bash

function send_notification() {
    volume=$(echo "$(pamixer --get-volume-human)" | sed "s/%//g")
    muted_hint=""
    if [ "$volume" == "muted" ]; then
        volume=$(echo "$(pamixer --get-volume)" | sed "s/%//g")
        muted_hint="-h string:bgcolor:#bbbbbb -h string:fgcolor:#444444 -h string:frcolor:#222222 MUTED" #-h string:hlcolor:#bbbbbb 
        echo $volume
        echo $muted_hint
    fi
    dunstify -a "changevolume" -u low -r 9981 -h int:value:"$volume" "Volume: $volume%" -t 1000 $muted_hint
}

case $1 in
up)
    pamixer --allow-boost -i 5
    send_notification $1
    ;;
down)
    pamixer --allow-boost -d 5
    send_notification $1
    ;;
mute)
    pamixer --toggle-mute
    send_notification $1
    ;;
esac
