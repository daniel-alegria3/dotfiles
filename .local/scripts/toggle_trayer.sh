#!/bin/sh
# Script called by dwm to show 'trayer', should use sxhkd instead
pgrep -x trayer >/dev/null && killall trayer || trayer --edge top --align right --widthtype request --margin 4 --padding 7 --SetPartialStrut true --expand true --tint 0x924441 --height 17 &
