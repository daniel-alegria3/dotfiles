# Exporting path variable here so sxhkd can access my scripts when being called
# by .xprofile
[[ -d "$HOME/.local/bin" ]] && export PATH="$PATH:$HOME/.local/bin"
[[ -d "$HOME/.local/scripts" ]] && export PATH="$PATH:$HOME/.local/scripts"
export ZDOTDIR="$HOME/.config/zsh"

